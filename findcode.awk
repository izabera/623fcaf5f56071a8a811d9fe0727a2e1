#!/usr/bin/gawk -E
# prints a sorted list of all the current processes
# sorted by the number of executable bytes in their address space

BEGIN {
    "echo /proc/[0-9]*/maps" | getline
    ARGC = split($0, ARGV)
}

BEGINFILE {
    if (ERRNO) nextfile
    file = gensub(/maps$/, "exe", 1, FILENAME)
    x = 0
    size = 0
}

/ ..x[ps] / {
    x++
    split($1, nums, /-/)
    size += strtonum("0x" nums[2]) - strtonum("0x" nums[1])
}

ENDFILE {
    maps[file] = FNR
    xmaps[file] = x
    bytes[file] = size
}

END {
    PROCINFO["sorted_in"] = "@val_num_asc"
    OFS = "\t"
    print "bytes", "xmaps", "maps", "pid", "name"
    for (file in bytes) {
        pid = gensub(/.proc.(.*).exe/, "\\1", 1, file)
        "readlink " file | getline
        name = $0
        if (name == "")
            name = file
        close("readlink " file)
        print bytes[file], xmaps[file], maps[file], pid, name
    }
}
